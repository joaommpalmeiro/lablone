# Notes

- https://github.com/joaopalmeiro/template-python-cli
- https://docs.gitlab.com/ee/api/rest/index.html
- https://docs.gitlab.com/ee/api/api_resources.html
- https://docs.gitlab.com/ee/api/projects.html:
  - https://docs.gitlab.com/ee/api/projects.html#list-all-projects
  - https://docs.gitlab.com/ee/api/projects.html#list-user-projects
  - https://docs.gitlab.com/ee/api/rest/index.html#keyset-based-pagination
- https://docs.gitlab.com/ee/api/repositories.html:
  - https://docs.gitlab.com/ee/api/repositories.html#get-file-archive
- https://github.com/python-gitlab/python-gitlab
- https://www.reddit.com/r/gitlab/comments/ve6p8g/how_to_get_a_list_of_all_your_gitlab_personal/
- https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html:
  - https://docs.gitlab.com/ee/api/rest/index.html#personalprojectgroup-access-tokens
  - https://gitlab.com/-/user_settings/personal_access_tokens
  - Scope: `read_api`
- https://github.com/encode/httpx/issues/815
- https://github.com/python-trio/trimeter
- https://github.com/mjpieters/aiolimiter
- https://trio.readthedocs.io/en/stable/reference-core.html#trio.CapacityLimiter
- https://github.com/florimondmanca/aiometer
- https://pakstech.com/blog/python-build-urls/

## Snippets

```python
from gaveta.json import write_json
write_json(repos, Path("repos.json"))
```
